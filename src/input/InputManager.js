class InputManager {

	constructor() {
		 this.m_cursors = game.input.keyboard.createCursorKeys();
	}

	get isDown() {
		return game.input.mousePointer.isDown;
	}

	get isUp() {
		return game.input.mousePointer.isUp;
	}

	get position() {
		return game.input.mousePointer.position;
	}

}