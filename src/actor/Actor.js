
class Actor {
	constructor() {
		
	}

	init() {
		this.dude = game.add.sprite(80 * scaleRatioX, game.world.height / 2 - 50 * scaleRatioY, 'kid');
        game.physics.arcade.enable(this.dude);
        this.dude.scale.setTo(0.5 * scaleRatioX);
        this.dude.animations.add('run', [14, 12, 10, 9, 0, 8], 10, true);
        this.dude.animations.add('death',[1, 3, 13], 12, false);
        var attack = this.dude.animations.add('attack',[6, 4, 5, 4, 5, 4, 5, 2], 12, false);
        attack.onStart.add(function() { this.isAttacking = true;}, this);
        attack.onComplete.add(function() { this.dude.play('run'); this.isAttacking = false;}, this);

        //this.dude.play('run');
        this.dude.body.gravity.y = 3000;
        this.jumpCount = 0;
        this.touchDown = false;
        this.isAttacking = false;
	}

	uninit() {
		this.dude.destroy();
	}

	get sprite() {
		return this.dude;
	}

	update() {

		if(this.dude.position.y > game.world.height) {
			gameManager.onGameOver();
			return;
		}

		if(gameManager.pauseGame)
			return;

		var width = game.world.width;
		var inputManager = gameManager.getInputManager();
		if(inputManager.isDown && !this.touchDown) {
			this.touchDown = true;
			var position = inputManager.position;
			if (position.x < width / 2)
		    {
		        console.log("Left down");
		        this.dude.play('attack');
		    }
		    else if (position.x > width / 2)
		    {
				console.log("Right down");
				if(this.dude.body.touching.down || this.jumpCount < 2) {
					this.jump();
					return;
				}
		    }
		}

		if(inputManager.isUp) {
			this.touchDown = false;
		}

		if(this.dude.body.touching.down)
			this.jumpCount = 0;

	    //game.debug.pointer(game.input.mousePointer);

	}

	jump() {
		this.dude.body.velocity.y = -1000;
		this.jumpCount++;	
	}
}