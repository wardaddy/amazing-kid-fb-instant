var MainState = {
	 preload: function()
	{
	    game.load.image('ground', 'assets/UI/bg.png');
	    game.load.image('ground_nails', 'assets/UI/bg_with_nails.png');
	    
	    game.load.image('edge', 'assets/UI/edge.png');

	    game.load.image('play', 'assets/UI/watch_ad.png');
	    
	    game.load.image('lb', 'assets/UI/leaderboard.png');
	    game.load.image('sfx_on', 'assets/UI/SFX_ON.png');
	    game.load.image('sfx_off', 'assets/UI/SFX_OFF.png');

	    game.load.image('home', 'assets/UI/home_icon.png');
	    game.load.image('restart', 'assets/UI/restart_icon.png');
	    
	    game.load.image('badge_0', 'assets/UI/badge_0.png');
	    game.load.image('badge_1', 'assets/UI/badge_1.png');
	    game.load.image('badge_2', 'assets/UI/badge_2.png');
	    game.load.image('badge_3', 'assets/UI/badge_3.png');

		game.load.image('close', 'assets/UI/close.png');
		game.load.image('profile', 'assets/UI/default_profile.jpg');
	    game.load.image('tutorial', 'assets/UI/Tutorial.png');

	    game.load.spritesheet('kid', 'assets/Actor/spritesheet.png', 256, 202);
	    game.load.spritesheet('dude', 'assets/Actor/sprite/running/spritesheet.png', 64, 100);
	    game.load.spritesheet('bat', 'assets/Enemy/flying/bat_spritesheet.png', 500, 256);
	    game.load.spritesheet('saw', 'assets/Enemy/rotating/saw_spritesheet.png', 213, 216);
	    game.load.spritesheet('butterfly', 'assets/Butterfly/spritesheet.png', 180, 205);
	    
	    game.load.bitmapFont('FFFForward', 'assets/font/font.png', 'assets/font/font.xml');

	    game.events.onAssetLoaded(10,10);
	    game.events.onGameReady();
    },

    create: function()
    {
    	var style = { font: "32px Arial", fill: "#ff0044", align: "center"};
    	this.title = game.add.text(5, game.world.height - 32, version, style);
		this.title.align = 'center';

        game.physics.startSystem(Phaser.Physics.ARCADE);
        gameManager = new GameManager();
    },

    update: function()
    {
        gameManager.update();
    }

}