class Platform {
	
	constructor(x, y, w, h, type) {
		this.platforms = game.add.group();
		this.platforms.enableBody = true;	

		this.edges = game.add.group();
        this.edges.enableBody = true;

        this.batGroup = game.add.group();
        this.batGroup.enableBody = true;
		
		this.sawGroup = game.add.group();
		this.sawGroup.enableBody = true;

		console.log("type :"+ type);
		if(type == 1)
			this.type_1(x, y, w, h);
		else if(type == 2)
			this.type_2(x, y, w, h);
		else if(type == 3)
			this.type_3(x,y,w,h);
		else if(type == 4)
			this.type_4(x,y,w,h);
		
	}

	type_1(x, y, w, h) {
		
		this.base = this.platforms.create(x, y, 'ground');
		this.base.enableBody = true;
		this.base.anchor.setTo(0, 1);		
        this.base.scale.setTo(w * scaleRatioX, h * scaleRatioY);
        this.base.body.immovable = true;

        this.left = this.edges.create(this.base.position.x - 10 * scaleRatioX, this.base.position.y + 10 *scaleRatioY, 'edge');
        this.left.enableBody = true;
        this.left.body.immovable = true;
        this.left.anchor.setTo(0, 1);
        this.left.scale.setTo(1 * scaleRatioX, h * scaleRatioY);
        this.left.alpha = 0;
	}

	type_2(x, y, w, h) {
		if(w > 2) w = 2;
		if(h < 1.5) h = 1.5;
		
		this.base = this.platforms.create(x, y, 'ground');
		this.base.enableBody = true;
		this.base.anchor.setTo(0, 1);		
        this.base.scale.setTo(w * scaleRatioX, h * scaleRatioY);
        this.base.body.immovable = true;

        this.left_1 = this.edges.create(this.base.position.x - 10 * scaleRatioX, this.base.position.y + 10 *scaleRatioY, 'edge');
        this.left_1.enableBody = true;
        this.left_1.body.immovable = true;
        this.left_1.anchor.setTo(0, 1);
        this.left_1.scale.setTo(1 * scaleRatioX, h * scaleRatioY);
        this.left_1.alpha = 0;

        // upper
        var distance = (Math.random() * (200) + 200) * scaleRatioY;
        this.base_2 = this.edges.create(x, this.base.position.y - this.base.height - distance, 'ground_nails');
        //var distance = Math.max(this.base_2.position.y - this.base.position.y, this.base.height);
        this.base_2.enableBody = true;
		this.base_2.anchor.setTo(0, 1);		
        this.base_2.scale.setTo(w * scaleRatioX, h * scaleRatioY);
        this.base_2.body.immovable = true;
	}

	type_3(x, y, w, h) {
		
		this.base = this.platforms.create(x, y, 'ground');
		this.base.enableBody = true;
		this.base.anchor.setTo(0, 1);		
        this.base.scale.setTo(w * scaleRatioX, h * scaleRatioY);
        this.base.body.immovable = true;

        this.left = this.edges.create(this.base.position.x - 10 * scaleRatioX, this.base.position.y + 10 *scaleRatioY, 'edge');
        this.left.enableBody = true;
        this.left.body.immovable = true;
        this.left.anchor.setTo(0, 1);
        this.left.scale.setTo(1 * scaleRatioX, h * scaleRatioY);
        this.left.alpha = 0;

        this.saw = this.sawGroup.create(this.base.position.x + this.base.width / 2, this.base.position.y - this.base.height, 'saw');
        this.saw.animations.add('rotate', [4,3,2,1,0], 120, true);
        this.saw.play('rotate');
		this.saw.enableBody = true;
		this.saw.anchor.setTo(0, 1);		
        this.saw.scale.setTo(0.3 * scaleRatioX, 0.3 * scaleRatioY);
        this.saw.body.immovable = true;
	}

	type_4(x, y, w, h) {
		
		this.base = this.platforms.create(x, y, 'ground');
		this.base.enableBody = true;
		this.base.anchor.setTo(0, 1);		
        this.base.scale.setTo(w * scaleRatioX, h * scaleRatioY);
        this.base.body.immovable = true;

        this.left = this.edges.create(this.base.position.x - 10 * scaleRatioX, this.base.position.y + 10 *scaleRatioY, 'edge');
        this.left.enableBody = true;
        this.left.body.immovable = true;
        this.left.anchor.setTo(0, 1);
        this.left.scale.setTo(1 * scaleRatioX, h * scaleRatioY);
        this.left.alpha = 0;

        this.bat = this.batGroup.create(this.base.position.x + this.base.width / 2, this.base.position.y - this.base.height, 'bat');
        this.bat.animations.add('fly', [0, 1], 6, true);
        this.bat.play('fly');
		this.bat.enableBody = true;
		this.bat.anchor.setTo(0, 1);		
        this.bat.scale.setTo(0.25 * scaleRatioX, 0.25 * scaleRatioY);
        this.bat.body.immovable = true;

	}

	destructor() {
		this.destroy();
	}

	destroy() {
		this.platforms.forEach(function(p) {
			p.destroy();
		});
		this.edges.forEach(function(p) {
			p.destroy();
		});
		this.batGroup.forEach(function(p) {
			p.destroy();
		});
		this.sawGroup.forEach(function(p) {
			p.destroy();
		});

		this.platforms.destroy();
		this.edges.destroy();
		this.batGroup.destroy();
		this.sawGroup.destroy();
	}

	update() {

		this.platforms.forEach(function(p) {
			p.position.x -= 6;
		});
		this.edges.forEach(function(p) {
			p.position.x -= 6;
		});
		this.batGroup.forEach(function(p) {
			p.position.x -= 6;

			if(gameManager.actor.dude.position.x > p.x) {
				gameManager.pausegame = true;
				var tween = game.add.tween(p).to(gameManager.actor.dude.position, 100).start();
			}
		});
		this.sawGroup.forEach(function(p) {
			p.position.x -= 6;
		});

	}

	get position() {
		return this.base.position;
	}

	get group() {
		return this.platforms;
	}

	get edgeGroup() {
		return this.edges;
	}

	get getbatGroup() {
		return this.batGroup;
	}

	get getsawGroup() {
		return this.sawGroup;
	}

	get baseSprite() {
		return this.base;
	}

}
