
class PlatformManager {

  constructor() {
    for (var i = 0; i < 1; ++i) {
        this.m_platformList = [];
        this.m_collidableObj = [];
        console.log("creating platforms");
    }
        
  }

  init() {
    for(var i = 0; i < 5; ++i) {
      this.spawnPlaform();
    }
  }

  uninit() {
    console.log("platform manager uninit : "+this.m_platformList.length);

    for(var i = 0; i < this.m_platformList.length; ++i) {
        var p = this.m_platformList[i];
        //this.m_platformList.splice(i, 1);
        p.destroy();
        //1delete this.m_platformList[i];
    }
    this.m_collidableObj = [];
    this.m_platformList = [];
  }

 spawnPlaform() {
   var x;
   var y; 
   var w;
   var h;
   var type = game.rnd.integer() % 4 + 1 ;

    if(this.m_platformList.length == 0)
    {
      x = 0;
      y = game.world.height + 10 * scaleRatioY;
      w = 3;
      h = 2.5;
      type = 1;
    }
    else
    {
      var p = this.m_platformList[this.m_platformList.length - 1];
      var sprite = p.baseSprite;
      var gap = Math.random() * (200) + 200;
      x = sprite.position.x + sprite.width+ gap * scaleRatioX;
      y = game.world.height + 10 * scaleRatioY;
      w = (Math.random() * (3 - 0) + 1) * scaleRatioX;
      h = (Math.random() * (2.5 - 0) + 1) * scaleRatioY;
    }

      var platform = new Platform(x, y, w, h, type);
      this.m_platformList.push(platform);
  }

  enableCollision(obj) {
    this.m_collidableObj.push(obj);
  }

  update() {

    if(gameManager.gameover)
      return;

      for(var i = 0; i < this.m_platformList.length; ++i) {
        var p = this.m_platformList[i];
        
        for(var j = 0; j < this.m_collidableObj.length; ++j) {
          var hitPlatform = game.physics.arcade.collide( p.group, this.m_collidableObj[j]);
        }
      }

    if(gameManager.pauseGame)
      return;

      for(var i = 0; i < this.m_platformList.length; ++i) {
        var p = this.m_platformList[i];
        
        for(var j = 0; j < this.m_collidableObj.length; ++j) {
          var hitPlatform = game.physics.arcade.collide( p.group, this.m_collidableObj[j]);
          if(game.physics.arcade.collide( p.edgeGroup, this.m_collidableObj[j], this.edgeCollisionHandler, null, this)) {
            console.log("Returning from edge group collision");
            gameManager.gameover = true;
            return;
          }

          if(game.physics.arcade.collide( p.getbatGroup, this.m_collidableObj[j], this.batCollisionHandler, null, this)) {
            console.log("Returning from Bat group collision");
            return;
          }

          if(game.physics.arcade.collide( p.getsawGroup, this.m_collidableObj[j], this.sawCollisionHandler, null, this)) {
            console.log("Returning from Saw group collision");
            return;
          }

        }

        p.update();
      }

      for(var i = 0; i < this.m_platformList.length; ++i) {
        var p = this.m_platformList[i];
        var pos = p.position;
        if(pos.x < -game.world.width - p.baseSprite.width * 0.5) {
          this.m_platformList.splice(i, 1);
          p.destroy();
          this.spawnPlaform();

          // increment score
          gameManager.updateScore();
        }
      }
    }

 edgeCollisionHandler(sprite_1, sprite_2) {
  if(gameManager.pauseGame)
    return;
  gameManager.pauseGame = true;
  console.log("Game Over");
  return true;
  //console.log("collision handler : ");
 }

 batCollisionHandler(sprite_1, sprite_2) {
  if(gameManager.pauseGame)
    return;

  if(gameManager.actor.isAttacking) {
    sprite_2.destroy();
  } else {
    gameManager.pauseGame = true;
    setTimeout(function() { gameManager.onGameOver(); }, 500);
    gameManager.actor.dude.play("death");
    console.log("Game Over");
  }
  return true;
  //console.log("collision handler : ");
 }

 sawCollisionHandler(sprite_1, sprite_2) {
  if(gameManager.pauseGame)
    return;
  gameManager.pauseGame = true;
  setTimeout(function() { gameManager.onGameOver(); }, 500);
  gameManager.actor.dude.play("death");
  console.log("Game Over");
  return true;
  //console.log("collision handler : ");
 }
}