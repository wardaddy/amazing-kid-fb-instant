const { ListView } = window.PhaserListView;
class LeaderboardPopup {

	constructor() {

	}

	activate() {
		gameManager.getLeaderboardCount();
		gameManager.syncLeaderboard();
		this.bg = game.add.sprite(game.world.width / 2 - 192 * scaleRatioX, game.world.height / 2 - 192 * scaleRatioY, 'ground');
		this.bg.scale.setTo(2 * scaleRatioX, 3 * scaleRatioY);

		var maskW = 600
	    var maskH = 200
	    var boxW = maskW
	    var boxH = 50
         this.listView = new ListView(game, game.world, new Phaser.Rectangle(this.bg.position.x + 16 * scaleRatioX, this.bg.position.y + 16 * scaleRatioY, this.bg.width - 32 * scaleRatioX, this.bg.height - 32 * scaleRatioY), {
      		direction: 'y'
   		 });

   		this.close = game.add.button(this.bg.position.x + this.bg.width - 50 * scaleRatioX, this.bg.position.y - 50 * scaleRatioY, 'close', this.onClose, this, 2, 1, 0);
    	this.close.scale.setTo(0.35 * scaleRatioX, 0.35 * scaleRatioY);
	}

	addEntry(entry) {
		

//         game.load.onFileComplete.add(this.fileComplete, this);
//		console.log("Entries : "+entries.length);
 //  		 for (var i = 0; i < entries.length; i++) {
		      let group = game.add.group();
		      //var entry = entries[i];
		      console.log("Name: "+entry.getPlayer().getName()+"	Rank:"+entry.getRank()+ "	score : "+entry.getScore()+"	Pic: "+entry.getPlayer().getPhoto()); // 2
		      //name
		      let name = game.add.bitmapText(75 * scaleRatioX, 16 * scaleRatioY, 'FFFForward',entry.getPlayer().getName(),16);
		      name.scale.setTo(scaleRatioX);
		      group.add(name, true);
			  
			  //score
    		  let score = game.add.bitmapText(275 * scaleRatioX, 16 * scaleRatioY, 'FFFForward',entry,getScore(),16);
    		  score.scale.setTo(0.75 * scaleRatioX);
			  group.add(score, true);

		      //image
		      let img = group.create(0, 0, 'profile');
		      img.scale.setTo(0.25 * scaleRatioX);
		      let h = boxH + Math.floor(Math.random()*100)
		      var url = 'https://i.pinimg.com/originals/b9/58/2d/b9582d806f57b4d8aab0655759d3cb34.jpg';

		      //var lbItem = new LeaderboardItem(0, 0, boxW/2, h/2, url, "Dev", 10);
		      this.listView.add(group)
//   		 }

	}

	deactivate() {
		this.bg.destroy();
		this.listView.destroy();
		this.close.destroy();
	}

	onClose() {
		this.deactivate();
	}

}