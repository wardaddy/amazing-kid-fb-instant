class ResultMenu {

	constructor() {
		
	}

	activate() {
		this.bg = game.add.sprite(game.world.width / 2 - 192 * scaleRatioX, game.world.height / 2 - 192 * scaleRatioY, 'ground');		
		this.bg.scale.setTo(2 * scaleRatioX, 3 * scaleRatioY);
    	
    	this.home = game.add.button(this.bg.position.x, this.bg.position.y + this.bg.height + 25 * scaleRatioY, 'home', this.onHome, this, 2, 1, 0);
    	this.home.scale.setTo(0.5 * scaleRatioX, 0.5 * scaleRatioY);

    	this.restart = game.add.button(this.bg.position.x + this.bg.width - 90 * scaleRatioX, this.bg.position.y + this.bg.height + 25 * scaleRatioY, 'restart', this.onRestart, this, 2, 1, 0);
    	this.restart.scale.setTo(0.5 * scaleRatioX, 0.5 * scaleRatioY);

    	this.score = game.add.bitmapText(this.bg.position.x +32 * scaleRatioX, this.bg.position.y + 32 * scaleRatioY, 'FFFForward','Score : \n'+gameManager.score,32);
		this.score.align = 'left';
    	this.score.anchor.set(0);
    	this.score.scale.setTo(scaleRatioX, scaleRatioY);

    	if(gameManager.highscore < gameManager.score) 
    	{
    		gameManager.highscore = gameManager.score;
    		console.log("On highscore : "+gameManager.highscore);
    		gameManager.saveData();
    		gameManager.submitScoreToLB();
    	}

    	this.highscore = game.add.bitmapText(this.bg.position.x + 32 * scaleRatioX, this.bg.position.y + this.bg.height /2 + 32 * scaleRatioY, 'FFFForward','HighScore : \n'+gameManager.highscore,32);
		this.highscore.align = 'left';
    	this.highscore.anchor.set(0);
    	this.highscore.scale.setTo(scaleRatioX, scaleRatioY);

    	this.badge = game.add.sprite(this.bg.position.x + this.bg.width/2, this.bg.position.y + 16 * scaleRatioY, 'badge_0');
    	this.badge.scale.setTo(scaleRatioX, scaleRatioY);
    	this.setBadge();
	}

	deactivate() {
		this.bg.destroy();
		this.home.destroy();
		this.restart.destroy();
		this.score.destroy();
		this.highscore.destroy();
		this.badge.destroy();
	}

	update() {

	}

	setBadge() {
		if(gameManager.score < 10)
			this.badge.loadTexture('badge_0');
		else if(gameManager.score < 20)
			this.badge.loadTexture('badge_1');
		else if(gameManager.score < 30)
			this.badge.loadTexture('badge_2');
		else
			this.badge.loadTexture('badge_3');
	}

	onHome() {
		gameManager.stateManager.push(gameManager.stateManager.STATE.MAINMENU);
	}

	onRestart() {
		gameManager.stateManager.push(gameManager.stateManager.STATE.GAMEPLAY);
	}
}