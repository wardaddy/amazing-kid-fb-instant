class GameplayMenu {
	constructor() {
		this.isTutorial = true;
	}

	activate() {
		gameManager.pauseGame = true;
		game.physics.arcade.isPaused = true; // pause physics

		gameManager.resetScore();
		gameManager.platformManager.init();
		gameManager.actor.init();
		gameManager.platformManager.enableCollision(gameManager.actor.sprite);
		this.tutorial = game.add.sprite(-10 * scaleRatioX, 90 * scaleRatioY, 'tutorial');
		this.tutorial.scale.setTo(0.5 * scaleRatioX, 0.5 * scaleRatioY);
		this.isTutorial = true;

		this.score = game.add.bitmapText(32 * scaleRatioX, 32 * scaleRatioY, 'FFFForward',gameManager.score,32);
		this.score.align = 'left';
    	this.score.anchor.set(0);
    	this.score.scale.setTo(scaleRatioX, scaleRatioY);
	}

	deactivate() {
		gameManager.platformManager.uninit();
		gameManager.actor.uninit();
		this.score.destroy();
	}

	update() {
		if(!this.isTutorial) {
			gameManager.platformManager.update();
			gameManager.actor.update();
		}

		if(gameManager.getInputManager().isDown && this.isTutorial) {
			this.isTutorial = false;
			this.tutorial.destroy();
			gameManager.pauseGame = false;
			gameManager.gameover = false;
			game.physics.arcade.isPaused = false;
			gameManager.actor.dude.play('run');
			gameManager.actor.touchDown = true;
		}
		this.score.setText(gameManager.score);
	}

}