class StateManager {

	constructor() {
		this.STATE = {"NONE":-1, "MAINMENU":0, "GAMEPLAY":1, "RESULTMENU":2};
		this.state = this.STATE.NONE;
		this.mainMenu = new MainMenu();
		this.gameplay = new GameplayMenu();
		this.resultMenu = new ResultMenu();
		this.leaderboardPopup = new LeaderboardPopup();
	}

	push(state) {
		console.log("push state : "+state);
		this.pop();
		this.state = state;
		console.log("current state : "+this.state);
		if(this.state == this.STATE.MAINMENU) {
			this.mainMenu.activate();		
		} else if(this.state == this.STATE.GAMEPLAY) {
			// push gameplay state
			this.gameplay.activate();
		} else if(this.state== this.STATE.RESULTMENU) {
			this.resultMenu.activate();
		}
	}

	pop() {
		if(this.state == this.STATE.MAINMENU) {
			// pop mainmenu
			this.mainMenu.deactivate();
		} else if(this.state == this.STATE.GAMEPLAY) {
			// pop gameplay state
			this.gameplay.deactivate();
		} else if(this.state == this.STATE.RESULTMENU) {
			// pop result state
			this.resultMenu.deactivate();
		}
	}

	update() {
		/*if(gameManager.gameover)
		{
			//this.push(this.STATE.RESULTMENU);
			//gameManager.gameover = false;
			gameManager.pauseGame = false;
		}*/

		if(this.state == this.STATE.MAINMENU) {
			this.mainMenu.update();		
		} else if(this.state == this.STATE.GAMEPLAY) {
			this.gameplay.update();
		} else if(this.state == this.STATE.RESULTMENU) {
			this.resultMenu.update();
		}	
	}
}