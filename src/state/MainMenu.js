//const { ListView } = window.PhaserListView;
class MainMenu {
	constructor() {
		
	}

	activate() {
		this.title = game.add.bitmapText(game.world.width * 0.5, 100 * scaleRatioY, 'FFFForward','Amazing\nKid',64);
		this.title.align = 'center';
    	this.title.anchor.set(0.5);
    	this.title.scale.setTo(scaleRatioX, scaleRatioY);
    	//this.title.scale.setTo(5);
    	console.log("Main Menu");

    	// play
    	this.play = game.add.button(game.world.centerX - 64 * scaleRatioX, game.world.height / 2, 'play', this.onPlay, this, 2, 1, 0);
    	this.play.scale.setTo(scaleRatioX, scaleRatioY);
    	//lb
    	this.leaderboard = game.add.button(this.play.position.x - this.play.width, this.play.position.y + this.play.height * 1.5, 'lb', this.onLeaderboard, this, 2, 1, 0);
    	this.leaderboard.scale.setTo(0.5 * scaleRatioX, 0.5 * scaleRatioY);
    	//sfx
	    this.sfx = game.add.button(this.play.position.x + this.play.width, this.play.position.y + this.play.height * 1.5, 'sfx_on', this.onSFX, this, 2, 1, 0);
    	this.sfx.scale.setTo(0.5 * scaleRatioX, 0.5 * scaleRatioY);
    	//player
    	this.dude = game.add.sprite(this.play.position.x - this.play.width * 0.5, this.play.position.y - this.play.height, 'dude');
        this.dude.animations.add('run', [0, 1, 2, 3, 4, 5], 10, true);    
        this.dude.play('run');
        this.dude.scale.setTo(scaleRatioX, scaleRatioY);
		//butterfly
		this.butterfly_1 = game.add.sprite(this.play.position.x + this.play.width * 0.5, this.play.position.y - this.play.height, 'butterfly');
        this.butterfly_1.animations.add('fly', [0, 1, 2, 3], 10, true);    
        this.butterfly_1.play('fly');
        this.butterfly_1.scale.setTo(0.15 * scaleRatioX, 0.15 * scaleRatioY);

    	this.butterfly_2 = game.add.sprite(this.play.position.x + this.play.width * 0.5 + 64 * scaleRatioX, this.play.position.y - this.play.height + 64 * scaleRatioY, 'butterfly');
        this.butterfly_2.animations.add('fly', [0, 1, 2, 3], 10, true);    
        this.butterfly_2.play('fly');
        this.butterfly_2.scale.setTo(0.15 * scaleRatioX, 0.15 * scaleRatioY);

    	this.butterfly_3 = game.add.sprite(this.play.position.x + this.play.width * 0.5 + 64 * scaleRatioX, this.play.position.y - this.play.height + 10 * scaleRatioY, 'butterfly');
        this.butterfly_3.animations.add('fly', [0, 1, 2, 3], 10, true);    
        this.butterfly_3.play('fly');
        this.butterfly_3.scale.setTo(0.25 * scaleRatioX, 0.25 * scaleRatioY);

	 	/*var maskW = 600
	    var maskH = 200
	    var boxW = maskW
	    var boxH = 50
         this.listView = new ListView(game, game.world, new Phaser.Rectangle(game.world.centerX - maskW/2, 120, maskW, 400), {
      		direction: 'y'
   		 });

   		 for (var i = 0; i < 500; i++) {
		      let color = Phaser.Color.getRandomColor()
		      let group = game.make.group(null)
		      let g = game.add.graphics(0, 0, group)
		      let h = boxH + Math.floor(Math.random()*100)
		      g.beginFill(color)
		       .drawRect(0, 0, boxW, h)

		      let txt = game.add.text(boxW/2, h/2, i, {font: "40px Arial", fill: "#000"}, group)
		      txt.anchor.set(.5)
		      let img = game.add.image(0, 0, group.generateTexture())
		      this.listView.add(img)
   		 }*/
	}

	deactivate() {
		this.title.destroy();
		this.play.destroy();
		this.leaderboard.destroy();
		this.sfx.destroy();
		this.dude.destroy();
		this.butterfly_1.destroy();
		this.butterfly_2.destroy();
		this.butterfly_3.destroy();
	}

	update() {

	}

	onPlay() {
		gameManager.stateManager.push(gameManager.stateManager.STATE.GAMEPLAY);
	}

	onLeaderboard() {
		console.log("clicked on leaderboard button");
		gameManager.stateManager.leaderboardPopup.activate();
	}

	onSFX() {
		console.log("clicked on SFX button");	
	}
}