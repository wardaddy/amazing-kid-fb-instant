
class GameManager {

  constructor() {
  		this.pauseGame = false;
  		this.gameover = false;
	  	var m_input = new InputManager();
	  	this.getInputManager = function() {return m_input;}

	    this.actor = new Actor();
	    this.platformManager = new PlatformManager();

	    this.stateManager = new StateManager();
	    this.stateManager.push(this.stateManager.STATE.MAINMENU);

	    this.score = 0;
	    this.highscore = 0;
	    this.loadSaveData();
	    console.log("savedata highscore : "+this.highscore);
	}

	update() {
		this.stateManager.update();      	
    }

    updateScore() {
    	this.score += 1;
    }

    resetScore() {
    	this.score = 0;
    }

    onGameOver() {
    	this.stateManager.push(this.stateManager.STATE.RESULTMENU);
    	this.gameover = true;
    }

    saveData() {
    	FBInstant.player
		  .setDataAsync({
		    'highscore': gameManager.highscore,
		  })
		  .then(function() {
		  	console.log("save data : "+gameManager.highscore);
		  });
    }

    loadSaveData() {
    	FBInstant.player
		  .getDataAsync(['highscore'])
		  .then(function(data) {
		  	console.log("load save data : "+data['highscore']);
		     if (typeof data['highscore'] != 'undefined') {
      			gameManager.highscore = data['highscore']
    		 }
    		 else {
    		 	gameManager.highscore = 0;
    		 }
  		});
    }

    submitScoreToLB() {
    	console.log("submit score to lb");

    	FBInstant.getLeaderboardAsync('global_2')
		  .then(function(leaderboard) {
		  	console.log("getLeaderboard async : "+leaderboard.getName());
		  	try {
		    	return leaderboard.setScoreAsync(gameManager.highscore);
			} catch(error) {
				console.log("LB Error : "+error.message);
			}
		  })
		  .then(function(entry) {
		    console.log(entry.getScore()); // 42
		    console.log(entry.getPlayer().getName()); // '{race: "elf", level: 3}'
		  }).catch(function(error) {
		  		console.log(error);
		  });
		
    }


    getLeaderboardCount() {
    	FBInstant.getLeaderboardAsync('global_2')
		  .then(function(leaderboard) {
		  		console.log("LB Name : "+leaderboard.getName());
		    return leaderboard.getEntryCountAsync();
		  })
		  .then(function(count) { console.log(count); })
		  .catch(function(error) {
		  		console.log("Get LB Count : "+error);
		  });; // 24
    }

    syncLeaderboard() {
		FBInstant.getLeaderboardAsync('global_2')
		  .then(function(leaderboard) {
		    return leaderboard.getPlayerEntryAsync();
		  }).then(function(entries) {
		  	console.log("Entries : "+entries.length);
		  	if(entries.length == undefined) {
		  		for(var i = 0; i < entries.length; ++i)
			    	gameManager.stateManager.leaderboardPopup.addEntry(entries[i]);
		  	} else {
		  		gameManager.stateManager.leaderboardPopup.addEntry(entries);
		  	}
		  })
		  .catch(function(error) {
		  		console.log("Sync LB : "+ error);
		  });
    }
}